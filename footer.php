<?php
if (!defined('__TYPECHO_ROOT_DIR__')) exit;

?>
    <!--footer-->
<?php if($this->options->footad && !empty($this->options->footad) ): ?><div class="branding branding-black">
	<div class="container">
		<?php $this->options->footad(); ?>
	</div>
</div><?php endif; ?>
<footer id="footer" class="footer">
	<div class="container">
		<?php if($this->options->flinks && !empty($this->options->flinks) ): ?>
		<div class="flinks"><?php /* 底部导航条 一般li格式 友情链接使用*/ ?>
			<strong>友情链接</strong>
			<ul class='xoxo blogroll'>
				<?php $this->options->flinks(); ?>
			</ul>
		</div><?php endif; ?>
		<?php if($this->options->fcode && !empty($this->options->fcode) ): ?><div class="fcode"><?php $this->options->fcode(); ?></div><?php endif; ?>
		<p>Copyright &copy; <?php echo date("Y"); ?> <a href="<?php $this->options ->siteUrl(); ?>">拾光博客</a> | <a href="https://www.hiai.top/index.php/archives/shenming.html" >免责申明</a> | Powered by <a href="http://typecho.org/">Typecho</a> |<a href="http://www.miitbeian.gov.cn" target="_blank">鄂ICP备19007924号-1</a></p>
		<div style="width:300px;margin:0 auto;padding-bottom: 7px;">
		 	<a target="_blank" href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=42011102003142" style="display:inline-block;text-decoration:none;height:20px;line-height:20px;"><img src="" style="float:left;"/><p style="float:left;height:20px;line-height:20px;margin: 0px 0px 0px 5px; color:#939393;"><img src="https://www.hiai.top/yuan/beian.png">鄂公网安备 42011102003142号</p></a>
		</div>
		 
		<p><img src="<?php $this->options ->siteUrl(); ?>/usr/themes/HIAI/img/yongxin.jpg"></p>
		<div class="hide"></div>	
	</div>
</footer>
<?php if ($this->options->useHighline == 'able'): ?>
<script src="//cdn.bootcss.com/highlight.js/9.9.0/highlight.min.js"></script>
<?php endif; ?>
<script>
window.jsui={
    www: '<?php $this->options ->siteUrl(); ?>',
    uri: '<?php echo rtrim($this->options ->themeUrl,"/");?>',
    ver: '0.1',
	roll: [<?php if (!empty($this->options->sitebar_fu)) {$text = $this->options->sitebar_fu;}
	else{$text='1,2';} $sf_arr = explode(",", $text);foreach($sf_arr as $val){echo '"'.$val.'",';} //侧边栏第几个模块浮动?>],
    ajaxpager: '0',
    url_rp: '<?php $this->options->adminUrl('login.php'); ?>'
};
</script>
  <!--百度统计-->
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?ce0a6af43c652c3267509e86227948e0";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>

<!-- 自动推送工具代码 -->

<script>
(function(){
    var bp = document.createElement('script');
    var curProtocol = window.location.protocol.split(':')[0];
    if (curProtocol === 'https') {
        bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
    }
    else {
        bp.src = 'http://push.zhanzhang.baidu.com/push.js';
    }
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(bp, s);
})();
</script>

  <!--公告栏-->
  <script type='text/javascript' src='<?php $this->options->themeUrl('js/libs/jquery-3.1.1.min.js'); ?>'></script>
  <script type="text/javascript">
    $(function(){
        var num=$(".qgg_scroll_list").find("li").length;
        if (num>1) {
            setInterval(function(){ 
            $('.qgg_scroll_list').animate({
                marginTop:"-26px"
            },500,function(){
                $(this).css({marginTop : "0"}).find("li:first").appendTo(this);
            });
        }, 6000);
        }        
    });
  </script>
<script> paceOptions = { elements: {selectors: ['#footer']}};</script>
<script src="//cdn.bootcss.com/pace/1.0.2/pace.min.js"></script>
<script type='text/javascript' src='//cdn.bootcss.com/bootstrap/3.2.0/js/bootstrap.min.js?ver=0.1'></script>
<script type='text/javascript' src='<?php $this->options->themeUrl('js/loader.js?ver=0.1'); ?>'></script>
<script src='<?php $this->options->themeUrl('js/effect.js'); ?>' type="text/javascript"></script>
<?php if($this->options->GoogleAnalytics): ?>
<?php $this->options->GoogleAnalytics(); ?>
<?php endif; ?>
</body>
</html>