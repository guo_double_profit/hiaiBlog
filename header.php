<?php if(!defined( '__TYPECHO_ROOT_DIR__'))exit;?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="<?php $this->options->charset(); ?>"><?php if ($this->options->DnsPrefetch): ?>
    <meta http-equiv="x-dns-prefetch-control" content="on"><?php if ($this->options->cdn_add): ?>
    <link rel="dns-prefetch" href="<?php $this->options->cdn_add(); ?>" /><?php endif; ?>
    <link rel="dns-prefetch" href="//cdn.bootcss.com" />
    <link rel="dns-prefetch" href="//secure.gravatar.com" />
	<link rel="dns-prefetch" href="//apps.bdimg.com"><?php endif; ?>
	<meta http-equiv="X-UA-Compatible" content="IE=11,IE=10,IE=9,IE=8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="keywords" content="拾光博客,个人技术博客,PHP,源码分享,知识共享,学习教程,java,jsp,python,编程学习" />
	<meta name="apple-mobile-web-app-title" content="<?php $this->options->title();?>">
	<meta http-equiv="Cache-Control" content="no-siteapp">
	<title><?php $this->archiveTitle(array('category'=>_t(' %s '),'search'=>_t(' %s '),'tag'=>_t(' %s '),'author'=>_t(' %s ')),'',' - ');?> <?php $this->options->title();?></title>
	<meta name='robots' content='noindex,follow' />
	<link rel='stylesheet' id='_bootstrap-css'  href='//cdn.bootcss.com/bootstrap/3.2.0/css/bootstrap.min.css?ver=0.1' type='text/css' media='all' />
	<link rel='stylesheet' id='_fontawesome-css'  href='//cdn.bootcss.com/font-awesome/4.2.0/css/font-awesome.min.css?ver=0.1' type='text/css' media='all' />
	<link rel='stylesheet' id='_main-css'  href='<?php $this->options->themeUrl('css/main.css?ver=0.1'); ?>' type='text/css' media='all' />
	<link rel='stylesheet' href='<?php $this->options->themeUrl('style.css'); ?>' type='text/css' media='all' />
	<script type='text/javascript' src='//cdn.bootcss.com/jquery/1.9.1/jquery.min.js?ver=0.1'></script>
	<!--加载进度条-->
	<link href="//cdn.bootcss.com/pace/1.0.2/themes/orange/pace-theme-flash.css" rel="stylesheet" />
	
	<!--加载进度条-->
	<link rel="shortcut icon" href="<?php $this->options->themeUrl('favicon.png'); ?>">
	<!--[if lt IE 9]><script src="http://apps.bdimg.com/libs/html5shiv/3.7/html5shiv.min.js"></script><![endif]-->
	<?php $this->header('keywords=&generator=&template=&pingback=&xmlrpc=&wlw=&commentReply=&rss1=&rss2=&atom='); ?>

</head>
<body class="<?php if ($this->is('index')) : ?>home<?php elseif ($this->is('post')) : ?>single<?php elseif ($this->is('page')) : ?>page<?php elseif ($this->is('archive')) : ?>archive<?php else: ?><?php endif; ?>">
<header class="header">
<div class="container">
	<h1 class="logo"><?php if (!empty($this->options->logoUrl)): ?>
        <a href="<?php $this->options ->siteUrl(); ?>">
            <img src="<?php $this->options->logoUrl(); ?>" alt="<?php $this->options->title();?>" />
		</a><?php else: ?>
		<a href="<?php $this->options ->siteUrl(); ?>">
            <img src="<?php $this->options->themeUrl('img/logo.png'); ?>" alt="<?php $this->options->title();?>" /><?php $this->options->title();?></a><?php endif; ?>
	</h1>
  <div class="brand"><?php if($this->options->logotext && !empty($this->options->logotext) ): ?><?php $this->options->logotext(); ?><?php endif; ?></div> <?php /* logo 旁边的两行说明 以<br>换行*/ ?>
  <ul class="site-nav site-navbar">
    <li>
      <a href="<?php $this->options ->siteUrl(); ?>">
        <i class="fa fa-home"></i>网站首页</a>
    </li><?php /* 头部大导航条 一般li格式 支持fa标签*/ ?>
    <li>
      <a><i class="fa fa-bars"></i>文章分类</a>
      <ul class="sub-menu"><?php /* 头部大导航条下二级菜单 一般li格式 */ ?>
      <?php 
      $this->widget('Widget_Metas_Category_List')->to($cats); 
      $i=0;  $b_arr = fa_ico(); 
      ?>
      <?php while ($cats->next()): ?>
      <li>
        <a href="<?php $cats->permalink()?>" target="_blank">
          <?php echo $b_arr[$i]; ?> 
          <?php $cats->name()?></a>
      </li>
      <?php $i++; ?>
      <?php endwhile; ?>
      </ul>
    </li>
    <li>
      <a href="https://www.hiai.top/yuan/" target="_blank">
        <i class="fa fa-leaf"></i>个人学习</a>
    </li>
    <li>
      <a>
        <i class="fa fa-file-text-o"></i>独立页面</a>
      <ul class="sub-menu"><?php /* 头部大导航条下二级菜单 一般li格式 */ ?>
        <?php $this->widget('Widget_Contents_Page_List')->to($pages); ?>
            <?php while($pages->next()): ?>
            <li <?php if($this->is('page', $pages->slug)): ?> class="current-menu-item"<?php endif; ?>><a href="<?php $pages->permalink(); ?>" target="_blank"><?php $pages->title(); ?></a></li>
            <?php endwhile; ?>
      </ul>
    </li>
    <li>
      <a href="https://www.hiai.top/xadmin" target="_blank">
        <i class="fa fa-user"></i>用户登录</a>
    </li>
    <li class="navto-search">
      <a href="javascript:;" class="search-show active">
        <i class="fa fa-search"></i>
      </a>
    </li>
  </ul>
  <i class="fa fa-bars m-icon-nav"></i>
</div>
</header>
<div class="site-search">
  <div class="container">
    <form method="get" class="site-search-form" action="<?php $this->options ->siteUrl(); ?>">
      <input class="search-input" name="s" type="text" placeholder="输入关键字" value="">
      <button class="search-btn" type="submit">
        <i class="fa fa-search"></i>
      </button>
    </form>
  </div>
</div><!--header-->
<div class="container">
  <div class="qgg_scroll">
    <ul class="qgg_scroll_list">
        <li><i class="fa fa-volume-up fa-lg" aria-hidden="true" style="color:#ff6666;padding-right: 10px;"></i>HIAI博客，2019-4-1公告插件正式上线。</li>
        <li><i class="fa fa-volume-up fa-lg" aria-hidden="true" style="color:#ff6666;padding-right: 10px;"></i>我们的宗旨：每天进步一点点。</li>
        <li><i class="fa fa-volume-up fa-lg" aria-hidden="true" style="color:#ff6666;padding-right: 10px;"></i>欢迎您的到来，让我们一起学习进步。</li>
        <li><i class="fa fa-volume-up fa-lg" aria-hidden="true" style="color:#ff6666;padding-right: 10px;"></i>基于Typecho主题Thug响应式主题上线。</li>
        <li><i class="fa fa-volume-up fa-lg" aria-hidden="true" style="color:#ff6666;padding-right: 10px;"></i>基于Typecho主题Besking响应式主题上线。</li>
    </ul>
</div>
</div>