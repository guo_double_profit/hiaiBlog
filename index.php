<?php
/**
 * 扒皮自大前端的WorePress主题。
 * 
 * @package DUX主题
 * @author 大前端
 * @version 0.1
 * @link http://www.daqianduan.com/
 */
if (!defined('__TYPECHO_ROOT_DIR__')) exit;
 $this->need('header.php');?>

<?php if (!empty($this->options->sidebarBlock) && in_array('ShowZazhi', $this->options->sidebarBlock)): ?>

  <section class="container">
    <div class="content-wrap">
      <div class="content"><?php if($this->_currentPage==1): ?>
  	<!--幻灯片开始--> 
  	<?php if ($this->options->Slider == 'SliderTrue'): ?>
  		<?php slout(); ?>
  	<?php endif; ?>
  	<!--幻灯片结束--><?php endif; ?>
      <?php hotpost(); ?> <?php //置顶文章 ?>
      <!--<div class="title">
          <h3>最新发布</h3>
  		<?php if($this->options->smallbanner && !empty($this->options->smallbanner) ): ?>
          <div class="more"><?php $this->options->smallbanner(); ?></div><?php endif; ?>
      </div>-->

      <?php $this->widget('Widget_Metas_Category_List')->to($categories); $i=0;  $b_arr = fa_ico(); ?>
      <?php while ($categories->next()): ?>
        <div class="widget widget-tops">
          <h3> <?php echo $b_arr[$i]; ?> <?php $categories->name(); ?><a href="<?php $categories->permalink(); ?>" style="float: right;">更多…</a></h3>
          <ul class="widget-navcontent">
            <li class="item item-01 active">
              <ul>
                <?php $this->widget('Widget_Archive@index-' . $categories->mid, 'pageSize=6&type=category', 'mid=' . $categories->mid)->to($posts); ?>
                <?php while ($posts->next()): ?>
                <li>
                  <span><?php $posts->date(); ?>  </span>
                  <a href="<?php $posts->permalink(); ?>">
                  <?php $posts->title(); ?></a><?php endwhile;?>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      <?php $i++; ?>
      <?php endwhile; ?>
      </div>
    </div>
  <?php $this->need('sidebar.php'); ?>
  </section>

<?php else: ?>

  <section class="container">
    <div class="content-wrap">
      <div class="content"><?php if($this->_currentPage==1): ?>
    <!--幻灯片开始--> 
    <?php if ($this->options->Slider == 'SliderTrue'): ?>
      <?php slout(); ?>
    <?php endif; ?>
    <!--幻灯片结束--><?php endif; ?>
      <?php hotpost(); ?> <?php //置顶文章 ?>
      <!--<div class="title">
          <h3>最新发布</h3>
      <?php if($this->options->smallbanner && !empty($this->options->smallbanner) ): ?>
          <div class="more"><?php $this->options->smallbanner(); ?></div><?php endif; ?>
      </div>-->
      <?php if($this->have()):?>
          <?php while($this->next()): ?>
        <article class="excerpt">
          <a class="focus" href="<?php $this->permalink() ?>">
            <img src="<?php $this->options->themeUrl('img/thumbnail.png'); ?>" data-src="<?php echo showThumb($this,null,true); ?>" class="thumb"></a>
          <header>
            <a class="cat"><?php $this->category(',',false); ?><i></i></a>
            <h2>
              <a href="<?php $this->permalink() ?>" title="<?php $this->title() ?>-<?php $this->options->title();?>"><?php  $this->title(); $this->sticky();?></a></h2>
          </header>
          <p class="meta">
            <time>
              <i class="fa fa-clock-o"></i><?php $this->date('Y-m-d'); ?></time>
            <span class="author">
              <i class="fa fa-user"></i><?php $this->author(); ?></span>
            <span class="pv">
              <i class="fa fa-eye"></i>阅读(<?php get_post_view($this) ?>)</span>
            <a class="pc" href="<?php $this->permalink() ?>#respond">
              <i class="fa fa-comments-o"></i>评论(<?php $this->commentsNum('0', '1', '%d'); ?>)</a>
          </p>
          <p class="note"><?php $this->excerpt(111, '...'); ?></p></article>
      <?php endwhile; ?>
      <?php endif; ?>   
      
        <div class="pagination">
      <?php $this->pageNav('上一页', '下一页', 3, '...', array('wrapTag' => 'ul', 'wrapClass' => 'page-navigator', 'itemTag' => 'li', 'textTag' => 'span', 'currentClass' => 'active', 'prevClass' => 'prev-page', 'nextClass' => 'next-page')); ?>
        </div>
      </div>
    </div>
  <?php $this->need('sidebar.php'); ?>
  </section>

<?php endif; ?>

<?php $this->need('footer.php'); ?>