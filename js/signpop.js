tbfine(function (){

return {
	init: function (){


	    if( $('#issignshow').length ){
	        jsui.bd.addClass('sign-show')
	        // $('.close-link').hide()
	        setTimeout(function(){
	        	$('#sign-in').show().find('input:first').focus()
	        }, 300);
	        $('#sign-up').hide()
	    }


	    $('.signin-loader').on('click', function(){
	    	jsui.bd.addClass('sign-show')
	    	setTimeout(function(){
            	$('#sign-in').show().find('input:first').focus()
            }, 300);
            $('#sign-up').hide()
	    })

	    $('.signup-loader').on('click', function(){
	    	jsui.bd.addClass('sign-show')
	    	setTimeout(function(){
            	$('#sign-up').show().find('input:first').focus()
            }, 300);
            $('#sign-in').hide()
	    })

	    $('.signclose-loader').on('click', function(){
	    	jsui.bd.removeClass('sign-show')
	    })

		$('.sign-mask').on('click', function(){
	    	jsui.bd.removeClass('sign-show')
	    })

	    $('.sign form').keydown(function(e){
			var e = e || event,
			keycode = e.which || e.keyCode;
			if (keycode==13) {
				$(this).find('.signsubmit-loader').trigger("click");
			}
		})

	    $('.signsubmit-loader').on('click', function(){
	    	if( jsui.is_signin ) return
                    
            var form = $(this).parent().parent()
            var inputs = form.serializeObject()
            var isreg = (inputs.action == 'signup') ? true : false

            if( !inputs.action ){
                return
            }

            if( isreg ){
            	if( !is_mail(inputs.email) ){
	                logtips('邮箱格式错误')
	                return
	            }

                if( !/^[a-z\d_]{3,20}$/.test(inputs.name) ){
                    logtips('昵称是以字母数字下划线组合的3-20位字符')
                    return
                }
            }else{
            	if( inputs.password.length < 6 ){
	                logtips('密码太短，至少6位')
	                return
	            }
            }

            $.ajax({  
                type: "POST",  
                url:  jsui.uri+'/action/log.php',  
                data: inputs,  
                dataType: 'json',
                success: function(data){  
                    // console.log( data )
                    if( data.msg ){
                        logtips(data.msg)
                    }

                    if( data.error ){
                        return
                    }

                    if( !isreg ){
                        location.reload()
                    }else{
                        if( data.goto ) location.href = data.goto
                    }
                }  
            });  
	    })

		var _loginTipstimer
		function logtips(str){
		    if( !str ) return false
		    _loginTipstimer && clearTimeout(_loginTipstimer)
		    $('.sign-tips').html(str).animate({
		        height: 29
		    }, 220)
		    _loginTipstimer = setTimeout(function(){
		        $('.sign-tips').animate({
		            height: 0
		        }, 220)
		    }, 5000)
		}

	}
}

})